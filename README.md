# BIE-IDO Example project for tutorials

This project serves as the initial repository for the tasks solved on the tutorials of BIE-IDO Introduction to DevOps.

## Credits
* **Author**: Nurken Samigullin
* **Tutorial**: tut1, 12.17


Task difficulty: simple

 Previous experience: none

